﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Bump : MonoBehaviour
{
    public AudioClip BumpClip;

    private AudioSource _sfxAudioSource;


    [Inject]
    private void Construct(
        [Inject(Id = "sfxAudioSource")] AudioSource audioSource)
    {
        _sfxAudioSource = audioSource;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
 

    private void OnCollisionEnter(Collision other)
    {
        _sfxAudioSource.PlayOneShot(BumpClip);
    }
}