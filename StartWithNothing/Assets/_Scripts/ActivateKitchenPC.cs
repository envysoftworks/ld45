﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateKitchenPC : MonoBehaviour
{
    public SummonPc PantryPC;
    
    private void OnTriggerStay(Collider other)
    {
        if ( Input.GetKeyDown(KeyCode.F))
        {
            PantryPC.Activate();
        }
    }
}
