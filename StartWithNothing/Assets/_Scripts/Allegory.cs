﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Allegory : MonoBehaviour
{
    private NarrationManager _narrationManager;
    private bool _playedClip = false;



    [Inject]
    private void Construct(NarrationManager narrationManager)
    {

        _narrationManager = narrationManager;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.F) && !_playedClip)
            {
                _playedClip = true; 
                _narrationManager.PlayClip(_narrationManager.AudioSettings._30_Door_allegory);
            }
        }
    }
}
