﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class BossEntranceTrigger : MonoBehaviour
{
    private UIManager _uiManager;
    private PlayerManager _playerManager;
    
    [Inject]
    private void Construct(UIManager uiManager, PlayerManager playerManager)
    {
        _uiManager = uiManager;
        _playerManager = playerManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.ActivateBossTimer(true);
            _playerManager.EnteredBossRoom();
            GetComponent<Collider>().enabled = false;
        }
    }
}
