﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Butter : MonoBehaviour
{
    private bool _canInteract = true;
    private UIManager _uiManager;
    private bool _isPickedUp = false;
    private PlayerManager _playerManager;

    private NarrationManager _narrationManager;

    [Inject]
    private void Construct(UIManager uiManager, PlayerManager playerManager, NarrationManager narrationManager)
    {
        _playerManager = playerManager;
        _uiManager = uiManager;
        _narrationManager = narrationManager;
    }


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_isPickedUp)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 1f,
                playerTransform.position.y+0.2f, playerTransform.position.z + playerTransform.forward.z * 1f);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (_canInteract && other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(true, "Press F to pick up.");
        }

        if (_isPickedUp && other.CompareTag("Cauldron"))
        {
            Debug.Log("GOT HERE!");
            _isPickedUp = false;
            other.GetComponent<Toaster>().AcceptButter(this);
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (_canInteract && other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(true, "Press F to pick up.");
            if (Input.GetKeyDown(KeyCode.F))
            {
                _narrationManager.PlayClip(_narrationManager.AudioSettings._44_butter);
                _isPickedUp = true;
                _canInteract = false;
                _uiManager.SetInstructions(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }
}
