﻿using System.Collections;
using System.Collections.Generic;
using Kino;
using UnityEngine;

public class GlitchManager : MonoBehaviour
{
    private AnalogGlitch _analogGlitch;
    private DigitalGlitch _digitalGlitch;
    private bool _isGlitching;


    private const float DigitalIntensityIntense = 0.145f;
    private const float AnalogScanLineJitterIntense = 0.379f;
    private const float AnalogVerticalJumpIntense = 0.129f;
    private const float AnalogHoprizontalShakeIntense = 0.35f;
    private const float AnalogColorDriftIntense = 0.19f;

    private const float DigitalIntensityLight = 0.02f;
    private const float AnalogScanLineJitterLight = 0.274f;
    private const float AnalogVerticalJumpLight = 0.0026f;
    private const float AnalogHoprizontalShakeLight = 0;
    private const float AnalogColorDriftLight = 0.05f;

    private void Awake()
    {
        _analogGlitch = GetComponent<AnalogGlitch>();
        _digitalGlitch = GetComponent<DigitalGlitch>();
    }

    // Start is called before the first frame update
    void Start()
    {
        ShowGlitch(false);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ShowGlitch(bool value, bool isIntense = false)
    {
        _analogGlitch.enabled = value;
        _digitalGlitch.enabled = value;
        _isGlitching = value;
        if (value)
        {
            if (isIntense)
            {
                _digitalGlitch.intensity = DigitalIntensityIntense;
                _analogGlitch.colorDrift = AnalogColorDriftIntense;
                _analogGlitch.scanLineJitter = AnalogScanLineJitterIntense;
                _analogGlitch.verticalJump = AnalogVerticalJumpIntense;
                _analogGlitch.horizontalShake = AnalogHoprizontalShakeIntense;
            }
            else
            {
                _digitalGlitch.intensity = DigitalIntensityLight;
                _analogGlitch.colorDrift = AnalogColorDriftLight;
                _analogGlitch.scanLineJitter = AnalogScanLineJitterLight;
                _analogGlitch.verticalJump = AnalogVerticalJumpLight;
                _analogGlitch.horizontalShake = AnalogHoprizontalShakeLight;
            }
        }
    }
}