﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Infinity : MonoBehaviour
{
    private InfinityCollider _collider1;
    private InfinityCollider _collider2;

    private PlayerManager _playerManager;
    private bool _audioPlayed = false;
    private NarrationManager _narrationManager;

    [Inject]
    private void Construct(PlayerManager playerManager, NarrationManager narrationManager)
    {
        _playerManager = playerManager;
        _narrationManager = narrationManager;
    }
    
    
    private void Awake()
    {
        _collider1 = GetComponentsInChildren<InfinityCollider>()[0];
        _collider2 = GetComponentsInChildren<InfinityCollider>()[1];
    }

    public void EnterInfinity(InfinityCollider infinityCollider)
    {
        if (infinityCollider == _collider1)
        {
            ChangePosition(_playerManager.transform,_collider2.transform , 1);
        }
        else
        {
            ChangePosition(_playerManager.transform,_collider1.transform, -1); 
        }
    }

    private void ChangePosition(Transform starting, Transform target,  float offset)
    {
        if (!_audioPlayed)
        {
            _audioPlayed = true;
            _narrationManager.PlayClip(_narrationManager.AudioSettings._25_Puke);
        }
        _playerManager.GetComponent<CharacterController>().enabled = false;
        _playerManager.FpsController.enabled = false;
        starting.position = new Vector3(target.position.x + offset,
            target.position.y, target.position.z + offset);
        _playerManager.GetComponent<CharacterController>().enabled = true;
        _playerManager.FpsController.enabled = true;
    }
}
