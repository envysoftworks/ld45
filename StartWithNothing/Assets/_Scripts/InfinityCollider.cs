﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfinityCollider : MonoBehaviour
{
    private Infinity _infinity;

    private void Awake()
    {
        _infinity = GetComponentInParent<Infinity>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _infinity.EnterInfinity(this);
        }
    }
    
}
