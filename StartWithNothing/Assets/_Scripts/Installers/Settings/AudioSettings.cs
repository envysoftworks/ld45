using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "AudioSettings", menuName = "Installers/AudioSettings")]
public class AudioSettings : ScriptableObjectInstaller<AudioSettings>
{
    public NarrationManager.Settings.Audio NarrationManagerAudioSettings;
    public PlayerManager.Settings.Audio PlayerManagerAudioSettings;
    public override void InstallBindings()
    {
        Container.BindInstance(NarrationManagerAudioSettings);
        Container.BindInstance(PlayerManagerAudioSettings);
    }
}