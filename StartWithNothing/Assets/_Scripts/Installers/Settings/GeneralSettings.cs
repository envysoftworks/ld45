using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GeneralSettings", menuName = "Installers/GeneralSettings")]
public class GeneralSettings : ScriptableObjectInstaller<GeneralSettings>
{
    public NarrationManager.Settings NarrationManagerSettings;
    public PlayerManager.Settings PlayerManagerSettings;
    public override void InstallBindings()
    {
        Container.BindInstance(NarrationManagerSettings);
        Container.BindInstance(PlayerManagerSettings);
    }
}