using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GraphicSettings", menuName = "Installers/GraphicSettings")]
public class GraphicSettings : ScriptableObjectInstaller<GraphicSettings>
{
    public override void InstallBindings()
    {
    }
}