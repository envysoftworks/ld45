﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CauldronItem : InteractibleItem
{
    public enum State
    {
        None,
        CanBePickedUp,
        IsFollowing,
        IsOnFire
    }

    public string FireTag;
    public State CauldronState { get; private set; }

    private void Start()
    {
        CauldronState = State.None;
    }
    
      private void Update()
    {
        Debug.Log(CauldronState);
        if (CauldronState==State.CanBePickedUp && Input.GetKeyDown(KeyCode.F) ) 
        {
            CauldronState = State.IsFollowing;
            _uiManager.SetInstructions(false);
        }
        if (CauldronState == State.IsFollowing)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 1.2f,
                playerTransform.position.y, playerTransform.position.z + playerTransform.forward.z * 1.2f);
        }
    }
      
    #region Triggers
    
    private void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
      
        if (other.CompareTag(FireTag) )
        {
            CauldronState = State.IsOnFire;
            transform.position = new Vector3(other.transform.position.x, other.transform.position.y + 0.2f,
                other.transform.position.z);
            transform.SetParent(other.transform); 
        }

        if (other.CompareTag("Frostkey"))
        {
            other.GetComponent<FrostKeyItem>().SetCauldron(this);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && CauldronState == State.None)
        {
            CauldronState = State.CanBePickedUp;
            _uiManager.SetInstructions(true, "Press F to pick-up.");
        }
    }
    
    private  void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && CauldronState == State.CanBePickedUp)
        {
            CauldronState = State.None;
            _uiManager.SetInstructions(false);
        }
    }
    
    #endregion
}