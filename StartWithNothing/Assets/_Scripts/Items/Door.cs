﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Door : MonoBehaviour
{
    public bool StartLocked;
    private UIManager _uiManager;
    private BoxCollider _boxCollider;
    private Animator _animator;
    private AudioSource _sfxAudioSource;
    public AudioClip DoorOpens;
    public AudioClip DoorCloses;
    public AudioClip DoorFails;
    private bool _isOpen = false;
    
    public bool IsLocked { get; private set; }

    [Inject]
    private void Construct(UIManager uiManager, [Inject(Id="sfxAudioSource")]AudioSource audioSource)
    {
        _uiManager = uiManager;
        _boxCollider = GetComponentInChildren<BoxCollider>();
        _animator = GetComponentInChildren<Animator>();
        _sfxAudioSource = audioSource;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        IsLocked = StartLocked;
    }

    public void Open()
    {
        if (!IsLocked)
        {
            _animator.SetTrigger("Open");
            _uiManager.SetInstructions(false);
            _sfxAudioSource.PlayOneShot(DoorOpens);
            _isOpen = true;
        }
    }

    public void Close()
    {
        if (_isOpen)
        {
            _animator.SetTrigger("Close");
            _uiManager.SetInstructions(false);
            _sfxAudioSource.PlayOneShot(DoorCloses);
            _isOpen = false;
            Lock();
        }
    }

    private void Fail()
    {
        _sfxAudioSource.PlayOneShot(DoorFails);
    }

    public void Unlock()
    {
        IsLocked = false;
    }

    public void Lock()
    {
        IsLocked = true;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(!_isOpen)
                _uiManager.SetInstructions(true, "Press F to open.");

            
            if (Input.GetKeyDown(KeyCode.F) && !_isOpen)
            {
                if(IsLocked)
                    Fail();
                else
                {
                    Open();
                }
            }
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }
}
