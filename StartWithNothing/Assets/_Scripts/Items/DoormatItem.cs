﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoormatItem : InteractibleItem
{
    private bool _canInteract = true;
    public UnityEvent OnDoormatRotated;
    public Key Key;

    private void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {

    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (_canInteract && other.CompareTag("Player"))
            {
                _uiManager.SetInstructions(true, "Press F to move doormat.");
            }
            if (_canInteract && Input.GetKeyDown(KeyCode.F))
            {
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + 48,
                    transform.eulerAngles.z);
                OnDoormatRotated?.Invoke();
                _canInteract = false;
                _uiManager.SetInstructions(false);
                Key.MakeInteractible();
                _narrationManager.PlayClip(_narrationManager.AudioSettings._16_The_Hits);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }
}