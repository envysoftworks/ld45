﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class FrostKeyItem : InteractibleItem
{
    public enum State
    {
        None,
        CanBePickedUp,
        IsFollowing,
        IsBoiled,
        IsBoiling,
        IsPlacedInKeyHole
    }

    public UnityEvent OnKeyPlaced; 
    public string ToasterTag;
    public string KeyHoleTag;
    public AudioClip PlacedClip;
    private State _state;
    private bool _toKeyHole;
    private CauldronItem _cauldron;
    private Transform _frostlock;
    private float _offset => _cauldron == null ? 0 : 0.5f;

    [Inject]
    private void Construct([Inject(Id = "frostlock")] Transform frostlock)
    {
        _frostlock = frostlock;
    }
    
    private void Start()
    {
        _state = State.None;
    }
    
    private void Update()
    {
        Debug.Log(_state);
        if (_state==State.CanBePickedUp && Input.GetKeyDown(KeyCode.F) ) //&& !_toKeyHole)
        {
            _state = State.IsFollowing;
            _uiManager.SetInstructions(false);
        }
        if (_state == State.IsFollowing)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 1.2f,
                playerTransform.position.y + _offset, playerTransform.position.z + playerTransform.forward.z * 1.2f);
        }
    }

    public void SetCauldron(CauldronItem cauldron)
    {
        _cauldron = cauldron;
        if (_cauldron.CauldronState == CauldronItem.State.IsOnFire)
        {
            _state = State.IsBoiling;
            transform.position = new Vector3(_cauldron.transform.position.x, _cauldron.transform.position.y + _offset,
                _cauldron.transform.position.z);
            _narrationManager.PlayClip(PlacedClip);
            _toKeyHole = true;
            StartCoroutine(WaitKeyToBoil());
        }
        else
        {
            transform.position = new Vector3(_cauldron.transform.position.x, _cauldron.transform.position.y + _offset,
                _cauldron.transform.position.z);
            _state = State.IsFollowing;
            transform.SetParent(_cauldron.transform);
        }
    }
    
    IEnumerator WaitKeyToBoil()
    {
        yield return new WaitForSeconds(5);
        _state = State.IsBoiled;
    }
    
    #region Triggers
    
    private void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
      
        if (other.CompareTag(KeyHoleTag) && _toKeyHole)
        {
            _state = State.IsPlacedInKeyHole;
            transform.position = new Vector3(other.transform.position.x, other.transform.position.y + _offset,
                other.transform.position.z);
            transform.SetParent(other.transform); // TODO: think about
            _narrationManager.PlayClip(PlacedClip);
            OnKeyPlaced?.Invoke();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && (_state == State.None || _state == State.IsBoiled))
        {
            _state = State.CanBePickedUp;
            _uiManager.SetInstructions(true, "Press F to pick-up.");
        }
        
        if (other.CompareTag(ToasterTag) && !_toKeyHole && _cauldron &&_cauldron.CauldronState == CauldronItem.State.IsOnFire
            && _state != State.IsBoiled)
        {
            _state = State.IsBoiling;
            transform.position = new Vector3(other.transform.position.x, other.transform.position.y + _offset,
                other.transform.position.z);
            _narrationManager.PlayClip(PlacedClip);
            _toKeyHole = true;
            StartCoroutine(WaitKeyToBoil());
        }
        
        if (other.CompareTag(ToasterTag) && !_toKeyHole && _cauldron &&_cauldron.CauldronState != CauldronItem.State.IsOnFire
            && _state != State.IsBoiled)
        {
            _state = State.IsBoiling;
            transform.position = new Vector3(other.transform.position.x, other.transform.position.y + _offset,
                other.transform.position.z);
            _narrationManager.PlayClip(PlacedClip);
            _toKeyHole = true;
            StartCoroutine(WaitKeyToBoil());
        }
    }
    
    private  void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && _state == State.CanBePickedUp)
        {
            _state = State.None;
            _uiManager.SetInstructions(false);
        }
    }
    
    #endregion
}
