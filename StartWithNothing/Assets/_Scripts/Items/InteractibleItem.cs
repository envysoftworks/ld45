﻿using UnityEngine;
using Zenject;

[RequireComponent(typeof(Collider))]
public class InteractibleItem : MonoBehaviour
{
    public AudioClip NarratorAudioClip;

    #region Dependencies

    protected NarrationManager _narrationManager;
    protected PlayerManager _playerManager;
    protected UIManager _uiManager;

    #endregion

    [Inject]
    protected void Construct(NarrationManager narrationManager, PlayerManager playerManager,
        UIManager uiManager)
    {
        _narrationManager = narrationManager;
        _playerManager = playerManager;
        _uiManager = uiManager;
    }
    
    private void PlayClip()
    {
        _narrationManager.PlayClip(NarratorAudioClip);
    }

    protected void OnTriggerEnter(Collider other)
    {
//        if (other.CompareTag("Player") && NarratorAudioClip)
//        {
//            PlayClip(); //TODO: call from child class
//        }
    }
}
