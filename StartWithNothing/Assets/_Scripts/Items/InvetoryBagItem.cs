﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class InvetoryBagItem : InteractibleItem
{
    private bool _canPick;
    
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _canPick = true;
            _uiManager.SetInstructions(true, "Press F to wear backpack.");
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _canPick = true;
            _uiManager.SetInstructions(true, "Press F to wear backpack.");
            if ( Input.GetKeyDown(KeyCode.F) && _canPick)
            {
                _uiManager.SetInstructions(false);
                _playerManager.PickedInventory();
                gameObject.SetActive(false);
                Destroy(this);
            }
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _canPick = false;
            _uiManager.SetInstructions(false);
        }
    }
}
