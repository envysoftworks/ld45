﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class KatanaItem : MonoBehaviour
{
    private bool _isPicked;
    public AudioClip PickedUpClip;
    public AudioClip Swoosh;
    private PlayerManager _playerManager;
    private UIManager _uiManager;
    private NarrationManager _narrationManager;
    private Animator _animator;

    private AudioSource _sfxAudioSource;

    [Inject]
    private void Construct(PlayerManager playerManager, UIManager uiManager,
        NarrationManager narrationManager, [Inject(Id = "sfxAudioSource")] AudioSource audioSource)
    {
        _playerManager = playerManager;
        _uiManager = uiManager;
        _narrationManager = narrationManager;
        _sfxAudioSource = audioSource;
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (_isPicked)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 1.2f,
                playerTransform.position.y, playerTransform.position.z + playerTransform.forward.z * 1.2f);
            if (Input.GetMouseButtonDown(0))
            {
                _animator.SetTrigger("hit");
                _sfxAudioSource.PlayOneShot(Swoosh);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_isPicked && Input.GetKeyDown(KeyCode.F))
        {
            _isPicked = true;
            _narrationManager.PlayClip(_narrationManager.AudioSettings._50_Katana);
            _uiManager.SetInstructions(false);
            _sfxAudioSource.PlayOneShot(PickedUpClip);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_isPicked)
        {
            _uiManager.SetInstructions(true, "Press F to pick-up.");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && !_isPicked)
        {
            _uiManager.SetInstructions(false);
        }
    }
}