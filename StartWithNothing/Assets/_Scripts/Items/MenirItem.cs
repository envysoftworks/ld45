﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using Zenject;

public class MenirItem : MonoBehaviour
{
    public enum MenirColor
    {
        R,
        G,
        B
    }


    [SerializeField] public MenirColor MenirCol;
    private int _count;
    private bool _playerIn;

    public AudioClip Activate;

    #region Dependencies

    private UIManager _uiManager;
    private MenirManager _menirManager;
    public BoxCollider TriggerCollider { get; private set; }
    public string DebugText;

    #endregion

    private AudioSource _sfxAudioSource;

    [Inject]
    private void Construct(UIManager uiManager, MenirManager menirManager,
        [Inject(Id = "sfxAudioSource")] AudioSource audioSource)
    {
        _uiManager = uiManager;
        _menirManager = menirManager;
        _sfxAudioSource = audioSource;
    }


    IEnumerator DebugMessage() //Todo: move timestamp to another function
    {
        for (int i = 0; i > -1; i++)
        {
            _uiManager.SetDebug(true,
                "[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "] " + DebugText +
                "\n\rUnityEngine.Debug:Log(Object)\n\r\n\r",
                true);


            yield return new WaitForSeconds(0.3f);
            if (i % 100 == 0)
            {
                _uiManager.SetDebug(true, "", false);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && _playerIn)
        {
            Debug.Log("f");
            _menirManager.MenirActivated(this);
            StopAllCoroutines();
            _uiManager.SetDebug(true,
                "[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "] " +
                MenirCol.ToString() + " Activated!" +
                "\n\rUnityEngine.Debug:Log(Object)\n\r\n\r",
                true);
            _sfxAudioSource.PlayOneShot(Activate);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerIn = true;
            if (MenirCol != MenirColor.B)
            {
                _uiManager.SetInstructions(true, "Press F to Activate.");
            }

            if (MenirCol == MenirColor.B)
                StartCoroutine(DebugMessage());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerIn = false;
            _uiManager.SetInstructions(false);
            StopAllCoroutines();
        }
    }


    private Renderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
        TriggerCollider = GetComponent<BoxCollider>();
    }

    IEnumerator ChangeColorOnActivation()
    {
        _renderer.material.color = Color.cyan;
        yield return new WaitForSeconds(0.5f);
        _renderer.material.color = Color.white;
    }
}