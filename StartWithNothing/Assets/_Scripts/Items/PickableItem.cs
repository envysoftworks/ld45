﻿using UnityEngine;
using UnityEngine.Events;

public class PickableItem : InteractibleItem
{
    public enum State
    {
        None,
        CanBePickedUp,
        IsFollowing,
        IsPlaced
    }

    public string MatchingTag;
    public AudioClip PlacedClip;
    private State _state;
    public PickableEvent OnPickablePlaced = new PickableEvent();

    private void Awake()
    {
    }

    private void Start()
    {
        _state = State.None;
    }

    private void Update()
    {
        if (_state == State.IsFollowing)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 0.8f,
                playerTransform.position.y, playerTransform.position.z + playerTransform.forward.z * 0.8f);
        }
    }

    #region Triggers

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(MatchingTag))
        {
            _state = State.IsPlaced;
            transform.SetParent(other.transform);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            //transform.position = new Vector3(other.transform.position.x, other.transform.position.y,
            //other.transform.position.z);

            OnPickablePlaced?.Invoke(this);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        base.OnTriggerEnter(other);
        if (other.CompareTag("Player") && _state != State.IsPlaced)
        {
            _uiManager.SetInstructions(true, "Press F to pick-up " + MatchingTag + ".");
            if (Input.GetKeyDown(KeyCode.F))
            {
                _state = State.IsFollowing;
                _uiManager.SetInstructions(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }

    #endregion

    [System.Serializable]
    public class PickableEvent : UnityEvent<PickableItem>
    {
    }
}