﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Key : MonoBehaviour
{
    private bool _canInteract = false;
    private bool _isHot = false;
    private UIManager _uiManager;
    private bool _isPickedUp = false;
    private PlayerManager _playerManager;
    private NarrationManager _narrationManager;
    
    public AudioClip KeyReady;
    private AudioSource _sfxAudioSource;


    [Inject]
    private void Construct(UIManager uiManager, PlayerManager playerManager, NarrationManager narrationManager, [Inject(Id="sfxAudioSource")]AudioSource audioSource)
    {
        _playerManager = playerManager;
        _uiManager = uiManager;
        _narrationManager = narrationManager;
        _sfxAudioSource = audioSource;
    }


    // Start is called before the first frame update
    void Start()
    {
    }

    public void MakeInteractible()
    {
        StartCoroutine(WaitForInteract());
    }

    private IEnumerator WaitForInteract()
    {
        yield return new WaitForSeconds(0.1f);
        _canInteract = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (_isPickedUp)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 1f,
                playerTransform.position.y, playerTransform.position.z + playerTransform.forward.z * 1f);
        }
    }

    public void StartHeatingUp()
    {
        if(!_isHot)
            StartCoroutine(HeatUp());
    }

    private IEnumerator HeatUp()
    {
        _narrationManager.PlayClip(_narrationManager.AudioSettings._17_Sure_why_not);
        yield return new WaitForSeconds(5);

        _canInteract = true;
        _isHot = true;
        _sfxAudioSource.PlayOneShot(KeyReady);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isPickedUp && !_isHot && other.CompareTag("Cauldron"))
        {
            Debug.Log("GOT HERE!");
            _isPickedUp = false;
            other.GetComponent<Toaster>().AcceptKey(this);
        }

        if (_isPickedUp && _isHot && other.CompareTag("FrostDoor"))
        {
            other.GetComponent<Door>().Unlock();
            other.GetComponent<Door>().Open();
            _narrationManager.PlayClip(_narrationManager.AudioSettings._19_Five_months);
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (_canInteract && other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(true, "Press F to pick up.");
            if (Input.GetKeyDown(KeyCode.F))
            {
                _isPickedUp = true;
                _canInteract = false;
                _uiManager.SetInstructions(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }
}