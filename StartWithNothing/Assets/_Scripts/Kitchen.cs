﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kitchen : MonoBehaviour
{
    public MeshRenderer Wall1;
    public MeshRenderer Wall2;
    public MeshRenderer Wall3;
    public MeshRenderer Wall5;
    public GameObject _peanut;
    public GameObject _butter;

    private void Awake()
    {
        _peanut = GameObject.FindGameObjectWithTag("peanut");
        _butter = GameObject.FindGameObjectWithTag("butter");
    }
    
    // Start is called before the first frame update
    void Start()
    {
        PainWalls(Color.white);
        _peanut.SetActive(false);
        _butter.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void PainWalls(Color color)
    {
        Wall1.material.color = color;
        Wall2.material.color = color;
        Wall3.material.color = color;
        Wall5.material.color = color;
    }
}
