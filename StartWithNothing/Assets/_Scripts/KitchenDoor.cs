﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenDoor : MonoBehaviour
{
    private Door _door;


    private void Awake()
    {
        _door = GetComponent<Door>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Unlock()
    {
        _door.Unlock();
    }

    public void Lock()
    {
        _door.Lock();
    }
}