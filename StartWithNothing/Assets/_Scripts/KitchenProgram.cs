﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenProgram : MonoBehaviour
{
    private Kitchen _kitchen;

    private void Awake()
    {
        _kitchen = FindObjectOfType<Kitchen>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetPeanut(int value)
    {
        if (value == 0)
        {
            _kitchen._peanut.SetActive(false);
            _kitchen._peanut.GetComponent<MeshRenderer>().enabled = false;
            _kitchen._peanut.GetComponent<SphereCollider>().enabled = false;
        }
        else
        {
            _kitchen._peanut.SetActive(true);
            _kitchen._peanut.GetComponent<MeshRenderer>().enabled = true;
            _kitchen._peanut.GetComponent<SphereCollider>().enabled = true;
        }
    }

    public void SetButter(int value)
    {
        if (value == 0)
        {
            _kitchen._butter.SetActive(false);
            _kitchen._butter.GetComponent<MeshRenderer>().enabled = false;
            _kitchen._butter.GetComponent<SphereCollider>().enabled = false;
        }
        else
        {
            _kitchen._butter.SetActive(true);
            _kitchen._butter.GetComponent<MeshRenderer>().enabled = true;
            _kitchen._butter.GetComponent<SphereCollider>().enabled = true;
        }
    }

    public void SetColor(int value)
    {
        switch (value)
        {
            case 0:
                _kitchen.PainWalls(Color.white);
                break;
            case 1:
                _kitchen.PainWalls(Color.red);
                break;
            case 2:
                _kitchen.PainWalls(Color.magenta);
                break;
            case 3:
                _kitchen.PainWalls(Color.blue);
                break;
            case 4:
                _kitchen.PainWalls(Color.cyan);
                break;
            case 5:
                _kitchen.PainWalls(Color.yellow);
                break;
            case 6:
                _kitchen.PainWalls(Color.black);
                break;
            case 7:
                _kitchen.PainWalls(Color.green);
                break;
        }
    }
}