﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

public class NarrationManager : MonoBehaviour
{
    #region Dependencies

    private Settings _settings;
    public Settings.Audio AudioSettings { get; private set; }

    #endregion

    private AudioSource _audioSource;

    [Inject]
    private void Construct(Settings settings, Settings.Audio audioSettings)
    {
        _settings = settings;
        AudioSettings = audioSettings;
    }

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public bool PlayClip(AudioClip audioClip)
    {
        if (!_audioSource.isPlaying)
        {
            _audioSource.PlayOneShot(audioClip);
            return true;
        }

        return false;
    }
    

    [Serializable]
    public class Settings
    {
   
        [Serializable]
        public class Audio
        {
            public List<AudioClip> IdleNarratorClips;

            public AudioClip _1_Alright_Dave;
            public AudioClip _2_Behold;
            
            public AudioClip _3_Footsteps;
            
            public AudioClip _4_A_Tutorial_Room;
            public AudioClip _5_Comic_Sans;
            public AudioClip _6_Inventory_Full;
            public AudioClip _7_Embarassing;
            public AudioClip _8_Think_outside_the_box;
            
            public AudioClip _9_Educate_yourself;
            public AudioClip _10_Breathtaking_Vista;
            public AudioClip _11_Whats_that_sound;
            public AudioClip _12_Not_Scary;
            public AudioClip _13_What_is_scary;
            
            public AudioClip _14_Underwhelming_Kitchen;
            public AudioClip _15_Toaster;
            public AudioClip _16_The_Hits;
            public AudioClip _17_Sure_why_not;
            public AudioClip _18_Boiling;
            
            public AudioClip _19_Five_months;
            public AudioClip _20_Four_monkeys;
            
            public AudioClip _21_Results;
            public AudioClip _22_Supposed_to_happen;
            public AudioClip _23_How_is_this_possible;
            
            public AudioClip _24_I_would_slap_it;
            public AudioClip _25_Puke;
            public AudioClip _26_Jim_Sterling;
            public AudioClip _27_The_fuck_is_this;
            public AudioClip _28_The_fuck_is_that; 
            public AudioClip _29_Nice_Table;
            public AudioClip _30_Door_allegory;
            public AudioClip _31_Promising_corridor;
            public AudioClip _32_Invisible_wall;
            public AudioClip _33_Twitter;
            
            public AudioClip _34_Empty_room;     
            public AudioClip _35_What;
            public AudioClip _36_Arbitrary_crap;
            public AudioClip _37_Big_guns;
            public AudioClip _38_Dont_touch_anything;
            public AudioClip _39_What_did_I_just_say;
            
            public AudioClip _40_Worse_off;
            public AudioClip _41_No_peanutbutter_here;
            public AudioClip _42_Sit__down_and_fix_this;
            public AudioClip _43_better_I_guess;
            public AudioClip _44_butter;
            public AudioClip _45_peanut;
            public AudioClip _46_Monkey;
            public AudioClip _47_priorities;
            
            public AudioClip _48_progress;
            public AudioClip _49_Matrix_yardsale;
            public AudioClip _50_Katana;
            
            public AudioClip _51_Dont_deserve_this;
            public AudioClip _52_Think_about_what_youve_done; 
            public AudioClip _53_Murmur;
        }
    }
}