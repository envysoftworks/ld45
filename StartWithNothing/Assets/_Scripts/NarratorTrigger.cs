﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;

public class NarratorTrigger : MonoBehaviour
{
    public AudioClip Clip;
    public float Delay;
    private NarrationManager _narrationManager;
    private bool _hasBeenTriggered;
    
    [Inject]
    private void Construct(NarrationManager narrationManager)
    {
        _narrationManager = narrationManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_hasBeenTriggered)
        {
            _hasBeenTriggered = true;                //TODO: in order not to start the coroutine multiple times
            StartCoroutine(WaitToPlayClip());
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
//        if (other.CompareTag("Player") && !_hasBeenTriggered)
//        {
//            _hasBeenTriggered = true;                //TODO: in order not to start the coroutine multiple times
//            StartCoroutine(WaitToPlayClip());
//        }
    }

    IEnumerator WaitToPlayClip()
    {
        yield return new WaitForSeconds(Delay);
        _hasBeenTriggered = _narrationManager.PlayClip(Clip);
    }
}
