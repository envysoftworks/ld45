﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Note : MonoBehaviour
{
   #region ForTheClip
   
   public Sprite NoteSprite;
   public AudioClip Clip;
   public float ClipDelay;
   private bool _clipPlayed;
   
   #endregion
   
   private SpriteRenderer _spriteRenderer;
   private bool _isOn;

   #region Dependencies

   private UIManager _uiManager;
   private NarrationManager _narrationManager;
   private PlayerManager _playerManager;
   
   #endregion

   [Inject]
   private void Construct(UIManager uiManager, NarrationManager narrationManager, PlayerManager playerManager)
   {
      _uiManager = uiManager;
      _narrationManager = narrationManager;
      _spriteRenderer = GetComponent<SpriteRenderer>();
      _spriteRenderer.sprite = NoteSprite;
      _playerManager = playerManager;
   }

   private void Start()
   {
      if (!Clip)
      {
         _clipPlayed = true;
      }
   }
   
   private void Update()
   {
      if (_isOn && Input.GetKeyDown(KeyCode.R))
      {
         _isOn = false;
         _uiManager.DisplayNoteImage(_isOn);
         _playerManager.UnfreezePlayer();
         Cursor.visible = false;
      }
   }

   private void OnTriggerStay(Collider other)
   {
      if (other.CompareTag("Player"))
      {
         _uiManager.SetInstructions(true, "Press F to read note.");


         if (Input.GetKeyDown(KeyCode.F))
         {
            _playerManager.FreezePlayer();
            _isOn = true;
            _uiManager.SetInstructions(false);
            _uiManager.DisplayNoteImage(_isOn, NoteSprite);
            if (!_clipPlayed)
            {
               _clipPlayed = true;
               StartCoroutine(WaitToPlayClip());
            }
         }
      }
   }


   private void OnTriggerExit(Collider other)
   {
      if (other.CompareTag("Player"))
      {
         _uiManager.SetInstructions(false);

      }
   }
   
   IEnumerator WaitToPlayClip()
   {
      yield return new WaitForSeconds(ClipDelay);
      _clipPlayed = _narrationManager.PlayClip(Clip);
   }
}
