﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PC : MonoBehaviour
{
    private GameObject _program;

    private UIManager _uiManager;
    private PlayerManager _playerManager;

    private bool _programIsOpen;

    public bool RunsKitchenProgram;

    private Pantry _pantryProgram;
    private Kitchen _kitchenProgram;
    private NarrationManager _narrationManager;

    [Inject]
    private void Construct(UIManager uiManager, PlayerManager playerManager, Pantry pantryProgram, Kitchen kitchenProgram, NarrationManager narrationManager)
    {
        _playerManager = playerManager;
        _uiManager = uiManager;
        _pantryProgram = pantryProgram;
        _kitchenProgram = kitchenProgram;
        _narrationManager = narrationManager;
    }


    // Start is called before the first frame update
    void Start()
    {
        if (RunsKitchenProgram)
        {
            _program = _kitchenProgram.gameObject;
        }
        else
        {
            _program = _pantryProgram.gameObject;
        }
}

    // Update is called once per frame
    void Update()
    {
        if (_programIsOpen && Input.GetKeyDown(KeyCode.R))
        {
            CloseProgram();
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !_programIsOpen)
        {
            _uiManager.SetInstructions(true, "Press F to fix the game.");
            if (Input.GetKeyDown(KeyCode.F))
            {
                _program.SetActive(true);
                _uiManager.SetInstructions(false);
                _playerManager.FreezePlayer();
                _programIsOpen = true;
                if(!RunsKitchenProgram)
                    _narrationManager.PlayClip(_narrationManager.AudioSettings._38_Dont_touch_anything);
            }
        }
    }

    private void CloseProgram()
    {
        _programIsOpen = false;
        _playerManager.UnfreezePlayer();
        _program.SetActive(false);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }
}