﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PCEnding : MonoBehaviour
{
   private UIManager _uiManager;

   [Inject]
   private void Construct(UIManager uiManager)
   {
      _uiManager = uiManager;
   }
   private void OnTriggerStay(Collider other)
   {
      if (other.CompareTag("Player"))
      {
         
         _uiManager.SetInstructions(true, "Press F to fix game.");
         #if UNITY_WEBGL
         _uiManager.SetInstructions(true, "Go to\n<b><color=green>bitbucket.org/envysoftworks/ld45/commits/all</color></b>\nto fix the game.");
         #endif
         if (Input.GetKeyDown(KeyCode.F))
         {
            #if !UNITY_WEBGL
               Application.OpenURL("https://bitbucket.org/envysoftworks/ld45/commits/all");
            #endif
         }
      }
   }
}
