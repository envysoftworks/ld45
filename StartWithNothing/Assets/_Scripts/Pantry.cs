﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pantry : MonoBehaviour
{
    private KitchenDoor[] _kitchenDoors;
    public Camel Camel;
    public MeshRenderer Wall1;
    public MeshRenderer Wall2;
    public MeshRenderer Wall3;
    public MeshRenderer Wall5;

    private void Awake()
    {
        Camel = GameObject.FindGameObjectWithTag("Camel").GetComponent<Camel>();
        _kitchenDoors = FindObjectsOfType<KitchenDoor>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        //TODO: When unlock door to find teh peanutbutter?
        ShowWall1(true);
        ShowWall2(true);
        ShowWall3(true);
        ShowWall5(true);
        Camel.SetCamel(false);
    }

    // Update is called once per frame
    private void Update()
    {
        
    }

    public void UnlockKitchenDoors()
    {
        foreach (var _kitchenDoor in _kitchenDoors)
        {
            _kitchenDoor.Unlock();
        }

    }
    
    public void LockKitchenDoors()
    {
        foreach (var _kitchenDoor in _kitchenDoors)
        {
            _kitchenDoor.Lock();
        }
    }


    public void ShowWall1(bool show)
    {
        Wall1.enabled = show;
    }
    
    public void ShowWall2(bool show)
    {
        Wall2.enabled = show;
    }
    
    public void ShowWall3(bool show)
    {
        Wall3.enabled = show;
    }
    
    public void ShowWall5(bool show)
    {
        Wall5.enabled = show;
    }
}




