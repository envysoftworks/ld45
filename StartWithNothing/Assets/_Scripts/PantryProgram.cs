﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantryProgram : MonoBehaviour
{
    private Pantry _pantry;
    private NarrationManager _narrationManager;
    private bool _clipPlayed = false;

    private void Awake()
    {
        _pantry = FindObjectOfType<Pantry>();
        _narrationManager = FindObjectOfType<NarrationManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void UnlockKitchenDoor(int value)
    {
        if (value == 0)
            _pantry.LockKitchenDoors();
        else
            _pantry.UnlockKitchenDoors();
    }

    public void ShowWall1(int value)
    {
        if (!_clipPlayed)
        {
            _clipPlayed=_narrationManager.PlayClip(_narrationManager.AudioSettings._39_What_did_I_just_say);
        }

        if (value == 0)
            _pantry.ShowWall1(true);
        else
            _pantry.ShowWall1(false);
    }

    public void ShowWall2(int value)
    {
        if (!_clipPlayed)
        {
            _clipPlayed=_narrationManager.PlayClip(_narrationManager.AudioSettings._39_What_did_I_just_say);
        }

        if (value == 0)
            _pantry.ShowWall2(true);
        else
            _pantry.ShowWall2(false);
    }

    public void ShowWall3(int value)
    {
        if (!_clipPlayed)
        {
            _clipPlayed=_narrationManager.PlayClip(_narrationManager.AudioSettings._39_What_did_I_just_say);
        }

        if (value == 0)
            _pantry.ShowWall3(true);
        else
            _pantry.ShowWall3(false);
    }

    public void ShowWall5(int value)
    {
        if (!_clipPlayed)
        {
            _clipPlayed=_narrationManager.PlayClip(_narrationManager.AudioSettings._39_What_did_I_just_say);
        }

        if (value == 0)
            _pantry.ShowWall5(true);
        else
            _pantry.ShowWall5(false);
    }

    public void SetCamel(int value)
    {
        if (!_clipPlayed)
        {
            _clipPlayed=_narrationManager.PlayClip(_narrationManager.AudioSettings._39_What_did_I_just_say);
        }

        if (value == 0)
            _pantry.Camel.SetCamel(false);
        else
            _pantry.Camel.SetCamel(true);
    }
}