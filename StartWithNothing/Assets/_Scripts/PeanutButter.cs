﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PeanutButter : MonoBehaviour
{
    private bool _canInteract = true;
    private UIManager _uiManager;
    private bool _isPickedUp = false;
    private PlayerManager _playerManager;
    private Rigidbody _rigidbody;
    private BoxCollider _boxCollider;
    private NarrationManager _narrationManager;

    private bool _clipsPlayed = false;

    [Inject]
    private void Construct(UIManager uiManager, PlayerManager playerManager, NarrationManager narrationManager)
    {
        _playerManager = playerManager;
        _uiManager = uiManager;
        _boxCollider = GetComponent<BoxCollider>();
        _rigidbody = GetComponent<Rigidbody>();
        _narrationManager = narrationManager;
    }


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (_isPickedUp)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 1f,
                playerTransform.position.y + 0.5f, playerTransform.position.z + playerTransform.forward.z * 1f);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (_isPickedUp && other.CompareTag("RustyDoor"))
        {
            other.GetComponent<Door>().Unlock();
            other.GetComponent<Door>().Open();
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (_canInteract && other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(true, "Press F to pick up.");
            if (Input.GetKeyDown(KeyCode.F))
            {
                _isPickedUp = true;
                _canInteract = false;
                _uiManager.SetInstructions(false);
                _rigidbody.isKinematic = true;
                _rigidbody.useGravity = false;
                _boxCollider.enabled = false;
                if (!_clipsPlayed)
                    StartCoroutine(PlaySounds());
            }
        }
    }

    private IEnumerator PlaySounds()
    {
        _clipsPlayed = true;
        _narrationManager.PlayClip(_narrationManager.AudioSettings._46_Monkey);
        yield return new WaitForSeconds(_narrationManager.AudioSettings._46_Monkey.length + 1);
        _narrationManager.PlayClip(_narrationManager.AudioSettings._47_priorities);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }
}