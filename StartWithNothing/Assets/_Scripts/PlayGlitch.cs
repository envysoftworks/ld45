﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayGlitch : MonoBehaviour
{
    private GlitchManager _glitchManager;
    private AudioSource _audioSource;
    private NarrationManager _narrationManager;
    private bool _glitchPlayed = false;
    private bool _glitchStarted = false;


    [Inject]
    private void Construct(GlitchManager glitchManager, NarrationManager narrationManager)
    {
        _glitchManager = glitchManager;
        _audioSource = GetComponent<AudioSource>();
        _narrationManager = narrationManager;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && _glitchStarted)
        {
            StartCoroutine(StopGlitch());
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(StartGlitch());
        }

    }


    private IEnumerator StartGlitch()
    {
        _glitchStarted = true;
        yield return new WaitForSeconds(1.5f);
        _glitchManager.ShowGlitch(true, true);
        _audioSource.Play();
        yield return new WaitForSeconds(0.2f);
        //_narrationManager.PlayClip(_narrationManager.AudioSettings._11_Whats_that_sound);
    }
    
    private IEnumerator StopGlitch()
    {
        if(!_glitchPlayed)
            _glitchPlayed = _narrationManager.PlayClip(_narrationManager.AudioSettings._12_Not_Scary);
        yield return new WaitForSeconds(5);
        _glitchManager.ShowGlitch(false, true);
        _audioSource.Stop();
        //yield return new WaitForSeconds(_narrationManager.AudioSettings._12_Not_Scary.length-4);
        //if(_glitchPlayed)
        //_narrationManager.PlayClip(_narrationManager.AudioSettings._13_What_is_scary);
        
    }
}