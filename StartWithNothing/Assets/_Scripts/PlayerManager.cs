﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using Zenject;
using Random = UnityEngine.Random;

public class PlayerManager : MonoBehaviour
{
    #region Dependencies

    private NarrationManager _narrationManager;
    private Settings _settings;
    private Settings.Audio _audioSettings;
    private UIManager _uiManager;
    private BossTimerUI _bossTimer;
    private Transform _teleportTarget;
    private RectTransform _princess;

    #endregion

    public AudioClip DamageTakenClip;
    private AudioSource _audioSource;
    private float _damageTimer;
    public FirstPersonController FpsController { get; private set; }
    private CharacterController _characterController;
    private bool _won = false;
    private bool _inBossRoom;
    private Rigidbody _rigidbody;
    private PCEnding _pcEnding;
    private Image _fadePanel;
    private bool _inventoryPicked;
    private RectTransform _inventoryPanel;

    private bool _inventoryPlayed = false;

    public PlayMusic EpicOST;

    public float Health { get; private set; }
    public float Stamina { get; private set; }

    private AudioSource _sfxAudioSource;
    public AudioClip Error;

    [Inject]
    private void Construct(NarrationManager narrationManager, Settings settings, Settings.Audio audioSettings,
        UIManager uiManager, BossTimerUI bossTimer, [Inject(Id = "princess")] RectTransform princess,
        PCEnding pcEnding, [Inject(Id = "fadePanel")] Image fadePanel,
        [Inject(Id = "teleportTarget")] Transform teleportTarget,
        [Inject(Id = "inventoryPanel")] RectTransform inventoryPanel,
        [Inject(Id = "sfxAudioSource")] AudioSource audioSource)
    {
        _narrationManager = narrationManager;
        _settings = settings;
        _audioSettings = audioSettings;
        _uiManager = uiManager;
        _bossTimer = bossTimer;
        _pcEnding = pcEnding;
        _fadePanel = fadePanel;
        _princess = princess;
        _teleportTarget = teleportTarget;
        _inventoryPanel = inventoryPanel;
        _characterController = GetComponent<CharacterController>();
        _sfxAudioSource = audioSource;
    }


    private void Awake()
    {
        FpsController = GetComponent<FirstPersonController>();
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
    }

/*
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Health--;
        }
    }
*/
    private void ReduceStamina()
    {
        Stamina -= Random.Range(0, Time.deltaTime * 22);
    }

    private void RefillStamina()
    {
        Stamina += Random.Range(0, Time.deltaTime * 50);
    }

    private void StaminaValue()
    {
        if (Input.GetKey(KeyCode.LeftShift) &&
            Stamina > 0)
        {
            if (Random.Range(0, 3) <= 0)
                ReduceStamina();
        }
        else if (Stamina < _settings.MaxStamina && !FpsController.CharMoved())
        {
            RefillStamina();
        }
    }

    private void Start()
    {
        _uiManager.SetMaxStats(_settings.MaxHealth, _settings.MaxStamina);
        Health = _settings.MaxHealth;
        Stamina = _settings.MaxStamina;
        StartCoroutine(FreezeAtStart());
        FpsController.m_UseHeadBob = false;
        GetComponent<AudioSource>().enabled = false;
        _narrationManager.PlayClip(_narrationManager.AudioSettings._1_Alright_Dave);
        _princess.gameObject.SetActive(false);
        _bossTimer.OnTimerEnded.AddListener(Win);
        _damageTimer = Random.Range(4f, 6f);
        _inventoryPanel.gameObject.SetActive(false);
    }

    void Update()
    {
        StaminaValue();

        if (_inBossRoom)
        {
            _damageTimer -= Time.deltaTime;
            if (_damageTimer <= 0)
            {
                TakeDamage();
            }
        }

        if (_inventoryPicked && Input.GetKeyDown(KeyCode.X))
        {
            _sfxAudioSource.PlayOneShot(Error);
            if (!_inventoryPlayed)
            {
                _narrationManager.PlayClip(_narrationManager.AudioSettings._6_Inventory_Full);
                _inventoryPlayed = true;
            }

            StartCoroutine(WaitInventoryPanel());
        }
    }


    public void PickedInventory()
    {
        _inventoryPicked = true;
    }

    IEnumerator WaitInventoryPanel()
    {
        _inventoryPanel.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        _inventoryPanel.gameObject.SetActive(false);
    }

    private void TakeDamage()
    {
        _damageTimer = Random.Range(3f, 4f);
        _audioSource.PlayOneShot(DamageTakenClip);
        ReduceHealth();
        _characterController.enabled = false;
        FpsController.enabled = false;
        transform.DOMove(new Vector3(transform.position.x - transform.forward.x + 0.8f, transform.position.y + 0.5f,
            transform.position.z - transform.forward.z + 0.8f
        ), 0.5f).OnComplete((delegate
        {
            _characterController.enabled = true;
            FpsController.enabled = true;
        }));
    }

    public void EnteredBossRoom()
    {
        _inBossRoom = true;
    }

    private void Win()
    {
        if (!_won)
        {
            EpicOST.StopMusic();
            StartCoroutine(PlayFinalClips());
            _won = true;
            _inBossRoom = false;
            _uiManager.ShowStats(false);
            _uiManager.ActivateBossTimer(false);
            _pcEnding.GetComponent<CapsuleCollider>().enabled = true;
            _princess.gameObject.SetActive(true);
            Debug.Log(_won);
        }
    }

    private IEnumerator PlayFinalClips()
    {
        _narrationManager.PlayClip(_narrationManager.AudioSettings._51_Dont_deserve_this);
        yield return new WaitForSeconds(_narrationManager.AudioSettings._51_Dont_deserve_this.length + 2);

        _fadePanel.DOFade(1, 1.5f).OnComplete(
            (delegate
            {
                _characterController.enabled = false;
                FpsController.enabled = false;
                transform.position = _teleportTarget.position;
                _fadePanel.DOFade(0, 1.5f).OnComplete(
                    (delegate
                    {
                        _characterController.enabled = true;
                        FpsController.enabled = true;
                    }));
            })
        );
        _narrationManager.PlayClip(_narrationManager.AudioSettings._52_Think_about_what_youve_done);
        yield return new WaitForSeconds(_narrationManager.AudioSettings._52_Think_about_what_youve_done.length);
        _narrationManager.PlayClip(_narrationManager.AudioSettings._53_Murmur);
    }

    private void ReduceHealth()
    {
        Health -= Random.Range(2, 5);
        if (Health <= 7)
        {
            Win();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("StatsActivation"))
        {
            _uiManager.ShowStats(true);
        }
    }


    public void FreezePlayer()
    {
        FpsController.enabled = false;
        _characterController.enabled = false;
        FpsController.SetCursotLock(false);
        Cursor.visible = true;
    }

    public void UnfreezePlayer()
    {
        FpsController.enabled = true;
        _characterController.enabled = true;
        FpsController.SetCursotLock(true);
    }

    IEnumerator FreezeAtStart()
    {
        yield return new WaitForSeconds(_narrationManager.AudioSettings._1_Alright_Dave.length);
        _narrationManager.PlayClip(_narrationManager.AudioSettings._2_Behold);
        yield return new WaitForSeconds(_narrationManager.AudioSettings._2_Behold.length);
        FpsController.m_UseHeadBob = true;
        GetComponent<AudioSource>().enabled = true;
        FpsController.m_WalkSpeed = 3.5f;
        yield return new WaitForSeconds(1);
        _narrationManager.PlayClip(_narrationManager.AudioSettings._3_Footsteps);
    }


    [Serializable]
    public class Settings
    {
        public float MaxIdlePlayerTime;
        public float MinIdlePlayerTime;
        public int MaxHealth;
        public int MaxStamina;
        public float FreezeAtStartTime;

        [Serializable]
        public class Audio
        {
            public List<AudioClip> IdlePlayerClips;
        }
    }
}