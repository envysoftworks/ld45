﻿using System.Collections;
using UnityEngine;

public class FrostLockManager : Riddle
{
    private bool _isInitialized;
    private FrostKeyItem _frostKey;
    private DoormatItem _doormat;
    private Door _exitDoor;

    private void Awake()
    {
        _frostKey = GetComponentInChildren<FrostKeyItem>();
        _doormat = GetComponentInChildren<DoormatItem>();
        _exitDoor = GetComponentInChildren<Door>();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _frostKey.OnKeyPlaced.AddListener(Solved);
        _doormat.OnDoormatRotated.AddListener(ActivateKey);
        _frostKey.gameObject.SetActive(false);
        _doormat.enabled = false;
    }

    private void ActivateKey()
    {
        _doormat.OnDoormatRotated.RemoveListener(ActivateKey);
        _doormat.GetComponent<BoxCollider>().enabled = false;
        _doormat.enabled = false;
        _uiManager.SetInstructions(false);
        _frostKey.gameObject.SetActive(true);
    }

    IEnumerator WaitForActivation()
    {
        yield return null;
        _frostKey.gameObject.SetActive(true);
    }

    protected override void Initialize()
    {
      
        _doormat.enabled = true;
    }

    protected override void Solved()
    {
        Debug.Log("Solved");
        _frostKey.OnKeyPlaced.RemoveListener(Solved);
        _exitDoor.GetComponent<BoxCollider>().enabled = false;
        _frostKey.enabled = false;
        //OpenDoor(); TODO: check it!!!!
        enabled = false;
    }
    
    private void OpenDoor()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_isInitialized)
        {
            Initialize();
            _isInitialized = true;
        }
    }
}