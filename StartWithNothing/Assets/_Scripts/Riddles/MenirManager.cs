﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MenirManager : Riddle
{
    private int _correctCount;
    public Door TempleDoor;
    private bool _playedSound;
    private AudioSource _sfxAudioSource;
    public AudioClip Failed;


    [Inject]
    private void Construct([Inject(Id = "sfxAudioSource")] AudioSource audioSource)
    {
        _sfxAudioSource = audioSource;
    }

    public void MenirActivated(MenirItem menir)
    {
        if (!_playedSound)
        {
            _narrationManager.PlayClip(_narrationManager.AudioSettings._22_Supposed_to_happen);
            _playedSound = true;
        }

        Debug.Log(menir.MenirCol);
        if (menir.MenirCol == MenirItem.MenirColor.R)
        {
            _correctCount = 1;
        }
        else if (_correctCount == 1 && menir.MenirCol == MenirItem.MenirColor.B)
        {
            _correctCount++;
        }
        else if (_correctCount == 2 && menir.MenirCol == MenirItem.MenirColor.G)
        {
            _correctCount++;
        }
        else
        {
            _correctCount = 0;
            _uiManager.SetDebug(true,
                "[" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "] " +
                "Failed! TRY AGAIN!" +
                "\n\rUnityEngine.Debug:Log(Object)\n\r\n\r",
                true);
            _sfxAudioSource.PlayOneShot(Failed);
        }

        if (_correctCount == 3)
        {
            // _uiManager.DeactivateDebug();
            Solved();
        }
    }

    private void Solved()
    {
        foreach (MenirItem menir in GetComponentsInChildren<MenirItem>())
        {
            menir.StopAllCoroutines();
            menir.TriggerCollider.enabled = false;
            menir.enabled = false;
        }

        Debug.Log("SOLVED!");
        StartCoroutine(DeactivateDebugPanel());
        TempleDoor.Unlock();
        TempleDoor.Open();
    }

    IEnumerator DeactivateDebugPanel()
    {
        yield return new WaitForSeconds(5);
        _uiManager.SetDebug(false);
    }
}