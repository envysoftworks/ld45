﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

public class Riddle : MonoBehaviour
{
    protected virtual void Solved(){}
    protected virtual void Initialize(){}

    #region Dependencies

    protected PlayerManager _player;
    protected UIManager _uiManager;
    protected NarrationManager _narrationManager;

    #endregion

    [Inject]
    private void Construct(PlayerManager player, UIManager uiManager, NarrationManager narrationManager)
    {
        _player = player;
        _uiManager = uiManager;
        _narrationManager = narrationManager;
    }
}
