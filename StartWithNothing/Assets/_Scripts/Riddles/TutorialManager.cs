﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : Riddle
{
    public AudioClip SolvedClip;
    private bool _isInitialized;
    private List<PickableItem> _shapes;
    private InvetoryBagItem _bag;
    private Door _exitDoor;

    private void Awake()
    {
        _shapes = new List<PickableItem>(GetComponentsInChildren<PickableItem>());
        _bag = GetComponentInChildren<InvetoryBagItem>();
        _exitDoor = GetComponentInChildren<Door>();
    }

    private void Start()
    {
        foreach (PickableItem item in _shapes)
        {
            item.OnPickablePlaced.AddListener(RemoveItem);
            item.enabled = false;
        }
    }

    private void RemoveItem(PickableItem item)
    {
        _shapes.Remove(item);
        Debug.Log(_shapes.Count);
        if (_shapes.Count == 2)
        {
            _narrationManager.PlayClip(_narrationManager.AudioSettings._7_Embarassing);
        }
    }

    protected override void Solved()
    {
        _exitDoor.Unlock();
        _exitDoor.Open();
        foreach (PickableItem item in _shapes)
        {
            item.enabled = false;
        }

        _narrationManager.PlayClip(_narrationManager.AudioSettings._8_Think_outside_the_box);
        enabled = false;
    }

    private void Update()
    {
        if (_shapes.Count == 0 && _bag == null)
        {
            Solved();
            Debug.Log("SOLVED");
        }
    }
    
    protected override void Initialize()
    {
        _isInitialized = true;
        foreach (PickableItem item in _shapes)
        {
            item.enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_isInitialized)
        {
            Initialize();
        }
    }
}