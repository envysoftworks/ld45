﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SavePoint : MonoBehaviour
{
    private PlayerManager _playerManager;
    private UIManager _uiManager;
    private bool _isSaving;
    private NarrationManager _narrationManager;

    [Inject]
    private void Construct(PlayerManager playerManager, UIManager uiManager, NarrationManager narrationManager)
    {
        _playerManager = playerManager;
        _uiManager = uiManager;
        _narrationManager = narrationManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !_isSaving)
        {
            _uiManager.SetInstructions(true, "Press F to save.");
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.F) && !_isSaving)
        {
            Save();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && !_isSaving)
        {
            _uiManager.SetInstructions(false);
        }
    }
    private void Save()
    {
        _narrationManager.PlayClip(_narrationManager.AudioSettings._48_progress);
        _isSaving = true;
        StartCoroutine(Saving());

    }

    IEnumerator Saving()
    {
        for (float i = 0; i <= 100; i += 0.5f)
        {
            _uiManager.SetInstructions(true, "Saving " + i.ToString("0.00") + "%");
            yield return null;
        }
        _uiManager.SetInstructions(true, "Saved progress successfully!!!!");
        yield return new WaitForSeconds(4);
        _isSaving = false;
        _uiManager.SetInstructions(false);
    }
  
}
