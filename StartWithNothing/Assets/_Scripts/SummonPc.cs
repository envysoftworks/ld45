﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

public class SummonPc : MonoBehaviour
{
    public GameObject DeskWithPc;
    public float Delay;
    private Door _door;
    public bool SpawnIfDoorIsLocked = false;
    public bool NeedsPrompt = false;
    private bool _canBeSummoned = false;
    private NarrationManager _narrationManager;
    
    private AudioSource _sfxAudioSource;
    public AudioClip DoorGlitches;


    [Inject]
    private void Construct([Inject(Id = "sfxAudioSource")] AudioSource audioSource)
    {
        _sfxAudioSource = audioSource;
    }


    private void Awake()
    {
        _narrationManager = FindObjectOfType<NarrationManager>();
        _door = GetComponent<Door>();
        _canBeSummoned = !NeedsPrompt;
    }

    public void Activate()
    {
        _canBeSummoned = true;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && _canBeSummoned)
        {
            if (Input.GetKeyDown(KeyCode.F) && (!_door.IsLocked || SpawnIfDoorIsLocked))
            {
                if (NeedsPrompt)
                {

                    _sfxAudioSource.PlayOneShot(DoorGlitches);

                    StartCoroutine(SummonWithDelay(_narrationManager.AudioSettings._37_Big_guns.length + 1));
                }
                else
                {
                    StartCoroutine(PlayClips());
                }

                _canBeSummoned = false;
            }
        }
    }

    private IEnumerator PlayClips()
    {
        _narrationManager.PlayClip(_narrationManager.AudioSettings._41_No_peanutbutter_here);
        yield return new WaitForSeconds(_narrationManager.AudioSettings._41_No_peanutbutter_here.length + 1);
        _narrationManager.PlayClip(_narrationManager.AudioSettings._42_Sit__down_and_fix_this);
        yield return new WaitForSeconds(_narrationManager.AudioSettings._42_Sit__down_and_fix_this.length + 1);

        DeskWithPc.SetActive(true);
    }

    private IEnumerator SummonWithDelay(float delay)
    {
        if (NeedsPrompt)
        {
            yield return new WaitForSeconds(DoorGlitches.length);
            _narrationManager.PlayClip(_narrationManager.AudioSettings._37_Big_guns);
        }
        yield return new WaitForSeconds(delay);
        DeskWithPc.SetActive(true);
    }
}