﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Toaster : MonoBehaviour
{
    #region Dependencies

    private PlayerManager _playerManager;
    private UIManager _uiManager;
    private NarrationManager _narrationManager;
    
    #endregion
    
    public AudioClip PickToasterClip;
    public AudioClip ToasterOnFireClip;
    private Key _key;
    private Peanut _peanut;
    private Butter _butter;
    public Transform KeyPosition;
    public Transform PeanutPosition;
    public Transform ButterPosition;
    public Rigidbody PeanutButter;
    private bool _isBoiling = false;
    private bool _isPickedUp = false;
    public Transform PositionOnFire;
    public AudioClip PeanutButterReady;
    private AudioSource _sfxAudioSource;

    [Inject]
    private void Construct(UIManager uiManager, PlayerManager playerManager, NarrationManager narrationManager, [Inject(Id="sfxAudioSource")]AudioSource audioSource)
    {
        _playerManager = playerManager;
        _uiManager = uiManager;
        _narrationManager = narrationManager;
        _sfxAudioSource = audioSource;
    }
    
    void Update()
    {
        if (_isPickedUp && !_isBoiling)
        {
            Transform playerTransform = _playerManager.transform;
            transform.position = new Vector3(playerTransform.position.x + playerTransform.forward.x * 1.2f,
                playerTransform.position.y, playerTransform.position.z + playerTransform.forward.z * 1.2f);
        }
    }

    public void AcceptKey(Key key)
    {
        _key = key;
        _key.transform.SetParent(KeyPosition);
        _key.transform.localPosition = Vector3.zero;
        _key.transform.localRotation = Quaternion.identity;
        if (_isBoiling)
        {
            _key.StartHeatingUp();
        }
    }

    public void AcceptPeanut(Peanut peanut)
    {
        _peanut = peanut;
        _peanut.transform.SetParent(PeanutPosition);
        _peanut.transform.localPosition = Vector3.zero;
        _peanut.transform.localRotation = Quaternion.identity;
        _peanut.transform.localScale = Vector3.one;
        if (_butter != null && _isBoiling)
            StartCoroutine(Mix());
    }

    public void AcceptButter(Butter butter)
    {
        _butter = butter;
        _butter.transform.SetParent(ButterPosition);
        _butter.transform.localPosition = Vector3.zero;
        _butter.transform.localRotation = Quaternion.identity;
        _butter.transform.localScale = Vector3.one;
        if (_peanut != null && _isBoiling)
            StartCoroutine(Mix());
    }

    private IEnumerator Mix()
    {
        yield return new WaitForSeconds(5);
        _peanut.gameObject.SetActive(false);
        _butter.gameObject.SetActive(false);
        PeanutButter.gameObject.SetActive(true);
        PeanutButter.isKinematic = false;
        PeanutButter.useGravity = true;
        PeanutButter.AddForce(0, 5f, 0, ForceMode.Impulse);
        _sfxAudioSource.PlayOneShot(PeanutButterReady);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (_isPickedUp && other.CompareTag("Fire"))
        {
            //_narrationManager.PlayClip(_narrationManager.AudioSettings._18_Boiling);
            transform.position = PositionOnFire.position;
            transform.rotation = PositionOnFire.rotation;
            _isBoiling = true;
            _isPickedUp = false;
            if (_key != null)
            {
                _key.StartHeatingUp();

            }

            if (_peanut != null && _butter != null)
                StartCoroutine(Mix());
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (!_isBoiling && !_isPickedUp && other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(true, "Press F to pick up.");
            if (Input.GetKeyDown(KeyCode.F))
            {
                
                _isPickedUp = true;
                _uiManager.SetInstructions(false);
                _narrationManager.PlayClip(_narrationManager.AudioSettings._15_Toaster);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _uiManager.SetInstructions(false);
        }
    }
}