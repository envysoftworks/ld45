﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BossTimerUI : MonoBehaviour
{
    public UnityEvent OnTimerEnded = new UnityEvent();
    private float _bossTimer = 45;
    private bool _isInitialized;
    private Text _text;

    private void Awake()
    {
        _text = GetComponentInChildren<Text>();
    }
    public void Initialize()
    { 
        _isInitialized = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isInitialized)
        {
            _bossTimer -= Time.deltaTime;
            _text.text = _bossTimer.ToString("0.00");
           
            if (_bossTimer <= 3.1f)
            {
             OnTimerEnded?.Invoke();
             enabled = false;
            }
        }
    }
}
