﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugUI : MonoBehaviour
{
    private TextMeshProUGUI _debugText;

    private void Awake()
    {
        _debugText = GetComponentInChildren<TextMeshProUGUI>();
    }

    public void SetDebugText(string text, bool isAdditive)
    {
        if (isAdditive)
         _debugText.text += text;
        else
            _debugText.text = text;
    }
}
