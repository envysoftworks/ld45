﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InstructionsUI : MonoBehaviour
{
    private Text _instructionsText;

    private void Awake()
    {
        _instructionsText = GetComponentInChildren<Text>();
    }

    public void SetInstructionsText(string text)
    {
        _instructionsText.text = text;
    }
}
