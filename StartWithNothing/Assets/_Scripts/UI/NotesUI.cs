﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesUI : MonoBehaviour
{
   private Image _image;

   private void Awake()
   {
      _image = GetComponentsInChildren<Image>()[1];
   }
   
   public void DisplayNoteImage(Sprite sprite)
   {
      _image.sprite = sprite;
      _image.preserveAspect = true;
   }
}
