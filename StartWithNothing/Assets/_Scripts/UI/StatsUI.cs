﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class StatsUI : MonoBehaviour
{
    private PlayerManager _player;
    private Slider _healthSlider;
    private Slider _staminaSlider;

    [Inject]
    private void Construct(PlayerManager player, [Inject(Id = "healthSlider")] Slider healthSlider,
        [Inject(Id= "staminaSlider")] Slider staminaSlider)
    {
        _player = player;
        _healthSlider = healthSlider;
        _staminaSlider = staminaSlider;
    }

    private void Update()
    {
        UpdateHealth();
        UpdateStamina();
    }

    public void SetMaxStats(int maxHealth, int maxStamina)
    {
        _healthSlider.maxValue = maxHealth;
        _staminaSlider.maxValue = maxStamina;
    }
    
    private void UpdateHealth()
    {
        _healthSlider.value = _player.Health;
    }

    private void UpdateStamina()
    {
        _staminaSlider.value = _player.Stamina;
    }
}
