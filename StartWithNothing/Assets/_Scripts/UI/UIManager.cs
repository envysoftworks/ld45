﻿using UnityEngine;
using Zenject;

public class UIManager : MonoBehaviour
{
    private InstructionsUI _instructions;
    private StatsUI _stats;
    private NotesUI _notes;
    private DebugUI _debug;
    private BossTimerUI _bossTimer;
    private Kitchen _kitchen;
    private Pantry _pantry;

    [Inject]
    private void Construct(InstructionsUI instructions, StatsUI stats, NotesUI notes, DebugUI debugUi, 
        BossTimerUI bossTimer, Kitchen kitchen, Pantry pantry)
    {
        _instructions = instructions;
        _stats = stats;
        _notes = notes;
        _debug = debugUi;
        _bossTimer = bossTimer;
        _kitchen = kitchen;
        _pantry = pantry;
    }

    private void Start()
    {
        SetInstructions(false);
        DisplayNoteImage(false);
        SetDebug(false);
        _bossTimer.gameObject.SetActive(false);
        ShowStats(false);
    }
    
    public void ActivateBossTimer(bool activate)
    {
        _bossTimer.gameObject.SetActive(activate);
        if (activate)
            _bossTimer.Initialize();  
    }

    public void ShowStats(bool show)
    {
        _stats.gameObject.SetActive(show);
    }
    
    public void DisplayNoteImage(bool isPanelActive, Sprite sprite = null)
    {
        _notes.gameObject.SetActive(isPanelActive);
        if (isPanelActive)
        {
            _notes.DisplayNoteImage(sprite);
        }
    }

    public void SetDebug(bool isPanelActive, string text = "", bool isAdditive = false)
    {
        _debug.gameObject.SetActive(isPanelActive);
        if (isPanelActive)
        {
            _debug.SetDebugText(text, isAdditive);
        }
    }

    public void DeactivateDebug()
    {
        _debug.gameObject.SetActive(false);
        _debug.enabled = false;
    }

    public void SetMaxStats(int maxHealth, int maxStamina)
    {
        _stats.SetMaxStats(maxHealth, maxStamina);
    }

    public void SetInstructions(bool isPanelActive, string text = "AAA")
    {
        _instructions.gameObject.SetActive(isPanelActive);
        if (isPanelActive)
        {
            _instructions.SetInstructionsText(text);
        }
    }

    public void ShowKitchen(bool value)
    {
        _kitchen.gameObject.SetActive(value);
    }


    public void ShowPantry(bool value)
    {
        _pantry.gameObject.SetActive(value);
    }
}